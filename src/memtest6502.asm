
#ifndef _MEMSTART
#define _MEMSTART $0007
#endif

#ifndef _MEMEND
#define _MEMEND $4000	
#endif

#ifndef _MEMSIZE
#define _MEMSIZE "??K"
#if _MEMEND = $1000
#define _MEMSIZE "4K "
#endif
#if _MEMEND = $2000
#define _MEMSIZE "8K "
#endif
#if _MEMEND = $4000
#define _MEMSIZE "16K"
#endif
#if _MEMEND = $8000
#define _MEMSIZE "32K"
#endif
#endif
	
	scrptr = $8000
	eoscr = $83e7		; last screen address
	lastline = $83c0	; start of last line

	memstart = _MEMSTART
	memend = _MEMEND

	addr = $0100
	aoff = $02
	ipass = $03
	pbyte = $04
	scroff = $0605

	
	.word $f000
	* = $f000

main:
	sei
	;; test the first few bytes of memory with $FF
	ldy #$00
	lda #$ff
zeroloopFF:	
	sta $00,Y
	cmp $00,Y
	beq zerookFF
	ldx #"Z"
	stx scrptr
	ldx #"F"
	stx scrptr+1
	sty scrptr+2
	sta scrptr+3
	lda $00,Y
	sta scrptr+4
	ldx #"Z"
	stx scrptr+5
	ldx #"F"
	stx scrptr+6
	jmp done
zerookFF:
	iny
	cpy #$10
	bne zeroloopFF
	
	;; test the first few bytes of memory with $00
	ldy #$00
	lda #$00
zeroloop00:	
	sta $00,Y
	cmp $00,Y
	beq zerook00
	ldx #"Z"
	stx scrptr
	ldx #"F"
	stx scrptr+1
	sty scrptr+2
	sta scrptr+3
	lda $00,Y
	sta scrptr+4
	ldx #"Z"
	stx scrptr+5
	ldx #"F"
	stx scrptr+6
	jmp done
zerook00:
	iny
	cpy #$10
	bne zeroloop00

	;; clear screen
	ldx #<scrptr
	stx <addr
	ldx #>scrptr
	stx >addr
	ldy #$00
	;; initialize screen pointer
	lda #<scrptr
	sta <scroff
	lda #>scrptr
	sta >scroff
clearloop:
	lda #" "
	sta (<addr),Y
	iny
	cpy #$00
	bne clearloop
	inx
	stx >addr
	cpx #$84
	bne clearloop

	;; print infotext
	ldy #$00
textloop:
	lda infotext,Y
	sta lastline,Y
	iny
	cpy #$22
	bne textloop

	;; set up first pass with $FF
	lda #$00
	sta ipass
	lda #"#"
	ldy #$00
	sta (<scroff),Y
	
pass:
	;; load start address
	ldx #>memstart
	ldy #<memstart
	;; set up screen ptr
	lda #$00
	sta <addr
	stx >addr
	sty aoff
	ldy ipass
	lda passbytes,Y
	sta pbyte
	sta eoscr

loop:
	ldx >addr
	ldy aoff
	;; break loop when end of DRAM is reached
	cpx #>memend
	bne noskip
	jmp passend

noskip:
	stx eoscr-2		; show current address as chars on screen
	sty eoscr-1
	;; store pass value at the current address
	lda pbyte
	;; special case $42: was already written, read only and compare to address' (HI xor LO)
	cmp #$42
	beq special42
	sta (<addr),Y
	;; compare current address to $FF, "continue" if equal
	cmp (<addr),Y
	bne print
	jmp loopend
	
special42:
	;; compare value in address to (HI xor LO)
	ldy aoff
	tya
	eor >addr
	cmp (<addr),Y
	bne print
	jmp loopend

print:	
	;; memory content was not equal, print

	;; space
	lda #" "
	ldy #$00
	sta (<scroff),Y
	;; increment
	ldy <scroff
	ldx >scroff
	iny
	cpy #$00
	bne nowrap1
	inx
	cpx #$83
nowrap1:	
	cpx #$83
	bne nowrap1a
	cpy #$98
	bne nowrap1a
	ldx #$80
	ldy #$00
nowrap1a:	
	sty <scroff
	stx >scroff

	;; upper nibble of X
	lda >addr
	lsr
	lsr
	lsr
	lsr
	tay
	lda hexchars,Y
	ldy #$00
	sta (<scroff),Y
	;; increment
	ldy <scroff
	ldx >scroff
	iny
	cpy #$00
	bne nowrap2
	inx
nowrap2:	
	cpx #$83
	bne nowrap2a
	cpy #$98
	bne nowrap2a
	ldx #$80
	ldy #$00
nowrap2a:	
	sty <scroff
	stx >scroff

	;; lower nibble of X
	lda >addr
	and #$0f
	tay
	lda hexchars,Y
	ldy #$00
	sta (<scroff),Y
	;; increment
	ldy <scroff
	ldx >scroff
	iny
	cpy #$00
	bne nowrap3
	inx
nowrap3:	
	cpx #$83
	bne nowrap3a
	cpy #$98
	bne nowrap3a
	ldx #$80
	ldy #$00
nowrap3a:	
	sty <scroff
	stx >scroff
	
	;; upper nibble of Y
	lda aoff
	lsr
	lsr
	lsr
	lsr
	tay
	lda hexchars,Y
	ldy #$00
	sta (<scroff),Y
	;; increment
	ldy <scroff
	ldx >scroff
	iny
	cpy #$00
	bne nowrap4
	inx
nowrap4:	
	cpx #$83
	bne nowrap4a
	cpy #$98
	bne nowrap4a
	ldx #$80
	ldy #$00
nowrap4a:	
	sty <scroff
	stx >scroff

	;; lower nibble of Y
	lda aoff
	and #$0f
	tay
	lda hexchars,Y
	ldy #$00
	sta (<scroff),Y
	;; increment
	ldy <scroff
	ldx >scroff
	iny
	cpy #$00
	bne nowrap5
	inx
nowrap5:	
	cpx #$83
	bne nowrap5a
	cpy #$98
	bne nowrap5a
	ldx #$80
	ldy #$00
nowrap5a:	
	sty <scroff
	stx >scroff
	
	;; address/value delimiter
	lda #">"
	ldy pbyte
	cpy #$42
	bne normaldelimiter
	lda #"X"
normaldelimiter:
	ldy #$00
	sta (<scroff),Y
	;; increment
	ldy <scroff
	ldx >scroff
	iny
	cpy #$00
	bne nowrap6
	inx
nowrap6:	
	cpx #$83
	bne nowrap6a
	cpy #$98
	bne nowrap6a
	ldx #$80
	ldy #$00
nowrap6a:	
	sty <scroff
	stx >scroff

	;; upper nibble of pass char
	lda pbyte
	cmp #$42
	bne normalupper
special42upper:
	lda aoff
	eor >addr
	lsr
	lsr
	lsr
	lsr
	tay
	lda hexchars,Y
	jmp upperendif
normalupper:
	lda ipass
	asl
	tay
	lda passchars,Y
upperendif:
	ldy #$00
	sta (<scroff),Y
	;; increment
	ldy <scroff
	ldx >scroff
	iny
	cpy #$00
	bne nowrap7
	inx
nowrap7:	
	cpx #$83
	bne nowrap7a
	cpy #$98
	bne nowrap7a
	ldx #$80
	ldy #$00
nowrap7a:	
	sty <scroff
	stx >scroff
	
	;; lower nibble of pass char
	lda pbyte
	cmp #$42
	bne normallower
special42lower:	
	lda aoff
	eor >addr
	and #$0f
	tay
	lda hexchars,Y
	jmp lowerendif
normallower:	
	lda ipass
	asl
	tay
	iny
	lda passchars,Y
lowerendif:
	ldy #$00
	sta (<scroff),Y
	;; increment
	ldy <scroff
	ldx >scroff
	iny
	cpy #$00
	bne nowrap8
	inx
nowrap8:	
	cpx #$83
	bne nowrap8a
	cpy #$98
	bne nowrap8a
	ldx #$80
	ldy #$00
nowrap8a:	
	sty <scroff
	stx >scroff

	;; upper nibble of actual value
	ldy aoff
	lda (<addr),Y
	lsr
	lsr
	lsr
	lsr
	tay
	lda hexchars,Y
	ldy #$00
	sta (<scroff),Y
	;; increment
	ldy <scroff
	ldx >scroff
	iny
	cpy #$00
	bne nowrap9
	inx
nowrap9:	
	cpx #$83
	bne nowrap9a
	cpy #$98
	bne nowrap9a
	ldx #$80
	ldy #$00
nowrap9a:	
	sty <scroff
	stx >scroff

	;; lower nibble of actual value
	ldy aoff
	lda (<addr),Y
	and #$0f
	tay
	lda hexchars,Y
	ldy #$00
	sta (<scroff),Y
	;; increment
	ldy <scroff
	ldx >scroff
	iny
	cpy #$00
	bne nowrap10
	inx
nowrap10:	
	cpx #$83
	bne nowrap10a
	cpy #$98
	bne nowrap10a
	ldx #$80
	ldy #$00
nowrap10a:	
	sty <scroff
	stx >scroff

	;; print cursor
	lda #"#"
	ldy #$00
	sta (<scroff),Y

loopend:	
	ldx >addr
	ldy aoff
	;; increment memory address
	iny
	cpy #$00
	bne loopend2		; branch on overflow (iny sets Z, but not V)
	inx			; increment y when x overflowed
loopend2:
	stx >addr
	sty aoff
	jmp loop
	
passend:
	ldy ipass
	lda passbytes,Y
	cmp #$7F
	bne passend2
	;; Done with all passes: Show = in status
	lda #"="
	sta eoscr-3
	jmp done
passend2:	
	iny
	sty ipass
	lda passbytes,Y
	cmp #$42
	beq fill
	jmp pass

	;; $42 is a special case meant to test proper adressing
	;; fill value behind each address with (HI xor LO)
fill:
	ldx #>memstart
	stx >addr
	ldy #<memstart
	sty aoff
fillloop:
	tya
	eor >addr
	sta (<addr),Y
	iny
	sty aoff
	cpy #00
	bne fillnocarry
	inx
fillnocarry:	
	stx >addr
	cpx #>memend
	bne fillloop
	jmp pass
	

	
hexchars:
	.asc "0123456789ABCDEF"
passbytes:
	.byt $FF, $00, $AA, $55, $42, $01, $02, $04, $08, $10, $20, $40, $80, $FE, $FD, $FB, $F7, $EF, $DF, $BF, $7F
passchars:
	.asc "FF00AA55XX0102040810204080FEFDFBF7EFDFBF7F"
infotext:
	.asc "CBM 3001 MEMTEST", _MEMSIZE, ", 2022 S3LPH.ME"

nmiv:
	;; NMI: Show ! in status
	lda #"!"
	sta eoscr-3
done:
	;; done, loop forever
	jmp done


eot:
	;; Fill with FF
	* = $fffa
	.dsb (*-eot), $ff
	;; 6502 vectors
	* = $fffa
	.byt <nmiv, >nmiv	; NMIV
	.byt <main, >main 	; RESV
	.byt <main, >main 	; IRQV
