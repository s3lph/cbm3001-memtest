
.PHONY: all memtest6502 emu6502

all: memtest6502 emu6502

clean:
	rm -rf tmp out

memtest6502:
	mkdir -p tmp out
	xa '-D_MEMEND=$$8000' -M -A F000 -O PETSCREEN -c -C -v -o tmp/memtest32k-f000.o65 src/memtest6502.asm
	dd if=tmp/memtest32k-f000.o65 bs=1 skip=2 of=out/memtest32k-f000.901465.bin
	xa '-D_MEMEND=$$4000' -M -A F000 -O PETSCREEN -c -C -v -o tmp/memtest16k-f000.o65 src/memtest6502.asm
	dd if=tmp/memtest16k-f000.o65 bs=1 skip=2 of=out/memtest16k-f000.901465.bin
	xa '-D_MEMEND=$$2000' -M -A F000 -O PETSCREEN -c -C -v -o tmp/memtest8k-f000.o65 src/memtest6502.asm
	dd if=tmp/memtest8k-f000.o65 bs=1 skip=2 of=out/memtest8k-f000.901465.bin
	xa '-D_MEMEND=$$1000' -M -A F000 -O PETSCREEN -c -C -v -o tmp/memtest4k-f000.o65 src/memtest6502.asm
	dd if=tmp/memtest4k-f000.o65 bs=1 skip=2 of=out/memtest4k-f000.901465.bin
	# Test image for the emulator built with ASCII charset instead of PETSCII
	xa '-D_MEMEND=$$1000' -M -A F000 -O ASCII -c -C -v -l tmp/memtest4k-f000-ascii.map -o tmp/memtest4k-f000-ascii.o65 src/memtest6502.asm
	dd if=tmp/memtest4k-f000-ascii.o65 bs=1 skip=2 of=out/memtest4k-f000-ascii.emu6502.bin

emu6502: memtest6502
	# _END is set to the address of the last instruction s.t. the emulator knows when to terminate
	gcc -D_END=$$(cat tmp/memtest4k-f000-ascii.map | grep ^done, | cut -d, -f2 | tr -d ' ,') -o out/emu6502 src/emu6502.c src/fake6502.c

test: memtest6502
	# Run the emulator, but terminate after 10M instructions
	gcc -D_LIMIT=10000000 -D_END=$$(cat tmp/memtest4k-f000-ascii.map | grep ^done, | cut -d, -f2 | tr -d ' ,') -o out/emu6502 src/emu6502.c src/fake6502.c
	out/emu6502 | tail -30
